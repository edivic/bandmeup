package com.example.bands.service;

import java.sql.Timestamp;
import java.util.List;

import com.example.bands.model.Statistics;

public interface StatisticsService {

	public List<Statistics> getStatistics(int id, Timestamp time);
	
}
