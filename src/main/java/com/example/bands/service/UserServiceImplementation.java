package com.example.bands.service;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.bands.model.Role;
import com.example.bands.model.User;
import com.example.bands.repository.RoleRepository;
import com.example.bands.repository.UserRepository;

@Service
public class UserServiceImplementation implements UserService {
	
	@Autowired
	BCryptPasswordEncoder encoder;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UserRepository userRepository;

	@Override
	public void saveUser(User user) {
		user.setPassword(encoder.encode(user.getPassword()));
		user.setStatus("VERIFIED");
		Role userRole = roleRepository.findByRole("SITE_USER");
		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		userRepository.save(user);
	}

	@Override
	public boolean isUserAlreadyPresent(User user) {
		if((userRepository.findByEmail(user.getEmail()) != null) || (userRepository.findByNickname(user.getNickname()) != null)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isNicknameAlreadyPresent(String nickname) {
		if(userRepository.findByNickname(nickname) != null) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isPasswordCorrect(String password, int id) {
		boolean ret = false;
		if(encoder.matches(password, userRepository.getPassword(id))) {
			ret = true;
		}
		return ret;
	}

	@Override
	public void updatePassword(String password, int id) {
		userRepository.updatePassword(encoder.encode(password), id);
		
	}
	
	

}