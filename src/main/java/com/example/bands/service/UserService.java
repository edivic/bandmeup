package com.example.bands.service;

import com.example.bands.model.User;

public interface UserService {

	public void saveUser(User user);
	
	public boolean isUserAlreadyPresent(User user);

	public boolean isNicknameAlreadyPresent(String nickname);
	
	public boolean isPasswordCorrect(String password, int id);
	
	public void updatePassword(String password, int id);
}
