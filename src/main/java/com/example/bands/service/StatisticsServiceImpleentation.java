package com.example.bands.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bands.model.Statistics;
import com.example.bands.repository.ReservationRepository;

@Service
public class StatisticsServiceImpleentation implements StatisticsService{
	
	@Autowired
	ReservationRepository reservationRepository;

	@Override
	public List<Statistics> getStatistics(int id, Timestamp time) {
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
    	//System.out.println(year);
    	int month = Calendar.getInstance().get(Calendar.MONTH);
    	//System.out.println(month+1);
		
		List<Statistics> stat = new ArrayList<Statistics>();
		
		for(int i = 0; i<=month; i++) {
			Statistics temp = new Statistics();
			temp.setMonth(monthName(i));
			temp.setGigs(reservationRepository.findBandsMonthlyStatistics(id, i+1, year, time));
			stat.add(temp);
		}
		return stat;
	}
	
	public static String monthName(int month){
	    String[] monthNames = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	    return monthNames[month];
	}

}
