package com.example.bands.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.NumberFormat;


@Entity
@Table(name = "band")
public class Band {
	
	@Id
	@OnDelete(action = OnDeleteAction.CASCADE)
	@SequenceGenerator(name = "seq_band", sequenceName = "band_band_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "seseq_bandq")
	@Column(name = "band_id")
	private int id;
	
	@NotNull(message = "Band name is obligatory")
	@Column(name = "band_name")
	private String name;
	
	@Email
	@NotNull(message = "Band email is obligatory")
	@Column(name = "email")
	private String email;
	
	@NotNull(message = "Band description is obligatory")
	@Column(name = "about")
	private String about;
	
	@NotNull(message = "Band city is obligatory")
	@Column(name = "city")
	private String city;
	
	@NotNull(message = "Number of members is obligatory")
	@Column(name = "members")
	private int members;
	
	@Column(name = "yt_url")
	private String ytUrl;
	
	@Column(name = "yt_channel")
	private String ytChannel;

	@Column(name = "img_url")
	private String imgUrl;
	
	@Column(name = "fb_url")
	private String fbUrl;
	
	@Column(name = "phone")
	@NumberFormat
	private String phone;
	
	@URL
	@Column(name = "band_page_url")
	private String bandPageUrl;
	
	@NotNull
	@Column(name = "created_by")
	private int createdBy;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "band_gig", joinColumns = @JoinColumn(name = "band_id"), inverseJoinColumns = @JoinColumn(name = "gig_id"))
	private Set<Gig> gigs;
	
	@Column(name = "rate_sum")
	private float rateSum;
	
	@Column(name = "no_rates")
	private float noRates;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public int getMembers() {
		return members;
	}

	public void setMembers(int members) {
		this.members = members;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int created_by) {
		this.createdBy = created_by;
	}

	public Set<Gig> getGigs() {
		return gigs;
	}

	public void setGigs(Set<Gig> gigs) {
		this.gigs = gigs;
	}

	public float getRateSum() {
		return rateSum;
	}

	public void setRateSum(float rateSum) {
		this.rateSum = rateSum;
	}

	public float getNoRates() {
		return noRates;
	}

	public void setNoRates(float noRates) {
		this.noRates = noRates;
	}

	public String getYtUrl() {
		return ytUrl;
	}

	public void setYtUrl(String ytUrl) {
		this.ytUrl = ytUrl;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getBandPageUrl() {
		return bandPageUrl;
	}

	public void setBandPageUrl(String bandPageUrl) {
		this.bandPageUrl = bandPageUrl;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getYtChannel() {
		return ytChannel;
	}

	public void setYtChannel(String ytChannel) {
		this.ytChannel = ytChannel;
	}
	

	public String getFbUrl() {
		return fbUrl;
	}

	public void setFbUrl(String fbUrl) {
		this.fbUrl = fbUrl;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
