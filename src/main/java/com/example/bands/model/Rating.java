package com.example.bands.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "band_ratings")
public class Rating {
	
	@Id
	@SequenceGenerator(name="seq_rating", sequenceName = "band_ratings_rating_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "seq_rating")
	@Column(name = "rating_id")
	private int id;
	
	@NotNull()
	@Column(name = "rating_band_id")
	private int bandId;
	
	@NotNull()
	@Column(name = "rating_user_id")
	private int userId;
	
	@Min(1)
	@Max(5)
	@NotNull(message = "Rating is obligatory")
	@Column(name = "rating")
	private float rating;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBandId() {
		return bandId;
	}

	public void setBandId(int bandId) {
		this.bandId = bandId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}
	
	
}
