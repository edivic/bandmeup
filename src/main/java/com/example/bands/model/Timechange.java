package com.example.bands.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "timechange")
public class Timechange {
	
	@Id
	@SequenceGenerator(name="seq_tc", sequenceName = "timechange_timechange_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "seq_tc")
	@Column(name = "timechange_id")
	private int id;
	
	@NotNull
	@Column(name = "reservation_id")
	private int reservationId;
	
	@NotNull
	@Column(name = "old_time")
	private Timestamp time;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}
	
}
