package com.example.bands.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "reservation")
public class Reservation {

	@Id
	@SequenceGenerator(name="seq_res", sequenceName = "reservation_reservation_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "seq_res")
	@Column(name = "reservation_id")
	private int id;
	
	@NotNull()
	@Column(name = "reservation_band_id")
	private int bandId;
	
	@NotNull()
	@Column(name = "reservation_user_id")
	private int userId;
	
	@NotEmpty(message = "Place is obligatory")
	@Column(name = "reservation_place")
	private String place;
	
	@NotEmpty(message = "Address is obligatory")
	@Column(name = "reservation_address")
	private String address;
	
	@Column(name = "reservation_time")
	private Timestamp time;
	
	@NotEmpty(message = "Reservation type is obligatory")
	@Column(name = "reservation_type")
	private String type;
	
	@NotEmpty(message = "Reservation description is obligatory")
	@Column(name = "reservation_description")
	private String description;

	@Column(name = "verified")
	private int verified;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBandId() {
		return bandId;
	}

	public void setBandId(int band_id) {
		this.bandId = band_id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int user_id) {
		this.userId = user_id;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVerified() {
		return verified;
	}

	public void setVerified(int verified) {
		this.verified = verified;
	}
	
	
}
