package com.example.bands.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "band_comments")
public class Comment {

	@Id
	@SequenceGenerator(name="seq_comment", sequenceName = "band_comments_comment_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "seq_comment")
	@Column(name = "comment_id")
	private int id;
	
	@NotNull()
	@Column(name = "band_id")
	private int bandId;
	
	/*@NotNull()
	@Column(name = "auth_user_id")
	private int userId;*/
	
	@NotNull(message = "Content is obligatory")
	@Column(name = "comment_content")
	private String content;
	
	@NotNull(message = "Timestamp is obligatory")
	@Column(name = "comment_time")
	private Timestamp time;
	
	@ManyToOne
	@JoinColumn(name = "auth_user_id", nullable=false, referencedColumnName="auth_user_id")
	private User auth_user;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBandId() {
		return bandId;
	}

	public void setBandId(int bandId) {
		this.bandId = bandId;
	}

	/*public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}*/

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public User getUser() {
		return auth_user;
	}

	public void setUser(User user) {
		this.auth_user = user;
	}
	
}
