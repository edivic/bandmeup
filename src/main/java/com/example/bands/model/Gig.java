package com.example.bands.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "gig")
public class Gig {
	
	@Id
	@SequenceGenerator(name="seq_gig", sequenceName = "gig_gig_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "seq_gig")
	@Column(name = "gig_id")
	private int id;
	
	@Column(name = "gig_type")
	private String type;
	
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "gigs")
	private Set<Band> bands;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
