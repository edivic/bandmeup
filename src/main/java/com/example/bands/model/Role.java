package com.example.bands.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "auth_role")
public class Role {

	@Id
	@SequenceGenerator(name="seq_role", sequenceName = "auth_role_auth_role_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "seq_role")
	@Column(name = "auth_role_id")
	private int id;
	
	@Column(name = "role_name")
	private String role;
	
	@Column(name = "role_desc")
	private String desc;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
