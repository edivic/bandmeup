package com.example.bands.model;

public class PasswordChange {

	private String oldPassword;
	
	private String newPassword;
	
	private String verifiedPassword;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getVerifiedPassword() {
		return verifiedPassword;
	}

	public void setVerifiedPassword(String verifiedPassword) {
		this.verifiedPassword = verifiedPassword;
	}
	
	
}
