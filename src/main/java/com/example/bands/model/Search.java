package com.example.bands.model;

import javax.validation.constraints.NotBlank;

public class Search {

	@NotBlank
	String str;

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}
	
	
}
