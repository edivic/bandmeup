package com.example.bands.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "auth_user")
public class User {

	@Id
	@SequenceGenerator(name="seq_user", sequenceName = "auth_user_auth_user_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "seq_user")
	@Column(name = "auth_user_id")
	private int id;
	
	@NotEmpty(message = "First name is obligatory")
	@Column(name = "first_name")
	private String name;
	
	@NotEmpty(message = "Last name is obligatory")
	@Column(name = "last_name")
	private String lastName;
	
	@NotEmpty(message = "Nickname is obligatory")
	@Column(name = "nickname", unique = true)
	private String nickname;
	
	@NotNull(message = "Email name is obligatory")
	@Email(message = "Email is invalid")
	@Column(name = "email", unique = true)
	private String email;
	
	@NotNull(message = "Password name is obligatory")
	@Length(min = 5, message = "Password must be at least 5 characters long")
	@Column(name = "password")
	private String password;

	@Column(name = "status")
	private String status;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "auth_user_role", joinColumns = @JoinColumn(name = "auth_user_id"), inverseJoinColumns = @JoinColumn(name = "auth_role_id"))
	private Set<Role> roles;

	@OneToMany(mappedBy = "auth_user")
	private Set<Comment> comments;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
	
}
