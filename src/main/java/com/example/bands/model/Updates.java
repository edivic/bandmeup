package com.example.bands.model;


public class Updates {

	private String nickname;
	
	private String description;

	private int members;
	
	private String ytUrl;
	
	private String ytChannel;
	
	private String imgUrl;
	
	private String bandPageUrl;
	
	private String fbUrl;
	
	private String phone;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getMembers() {
		return members;
	}

	public void setMembers(int members) {
		this.members = members;
	}

	public String getYtUrl() {
		return ytUrl;
	}

	public void setYtUrl(String ytUrl) {
		this.ytUrl = ytUrl;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getBandPageUrl() {
		return bandPageUrl;
	}

	public void setBandPageUrl(String bandPageUrl) {
		this.bandPageUrl = bandPageUrl;
	}

	public String getYtChannel() {
		return ytChannel;
	}

	public void setYtChannel(String ytChannel) {
		this.ytChannel = ytChannel;
	}
	

	public String getFbUrl() {
		return fbUrl;
	}

	public void setFbUrl(String fbUrl) {
		this.fbUrl = fbUrl;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
}
