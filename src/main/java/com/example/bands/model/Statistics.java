package com.example.bands.model;

public class Statistics {

	private String month;
	
	private int gigs;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getGigs() {
		return gigs;
	}

	public void setGigs(int gigs) {
		this.gigs = gigs;
	}
	
	
}
