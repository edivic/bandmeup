package com.example.bands.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.bands.configuration.EmailConfiguration;
import com.example.bands.model.Band;
import com.example.bands.model.Search;
import com.example.bands.model.User;
import com.example.bands.repository.BandRepository;
import com.example.bands.service.UserService;



@Controller
public class AuthenticationController {
	
	private EmailConfiguration emailConfig;
	
	public AuthenticationController(EmailConfiguration emailConfig) {
		this.emailConfig = emailConfig;
	}
	
	@Autowired
	UserService userService;
	
	@Autowired
	private BandRepository bandRepository;
	

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login"); // resources/template/login.html
		return modelAndView;
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView register() {
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		modelAndView.addObject("user", user); 
		modelAndView.setViewName("register"); // resources/template/register.html
		return modelAndView;
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView registerUser(@Valid User user, BindingResult bindingResult, ModelMap modelMap) {
		ModelAndView modelAndView = new ModelAndView();
		if(bindingResult.hasErrors()) {
			modelAndView.addObject("successMessage", "Please correct the errors in form!");
			modelMap.addAttribute("bindingResult", bindingResult);
			modelAndView.addObject("user", user);
		}
		else if(userService.isUserAlreadyPresent(user)){
			modelAndView.addObject("successMessage", "User with given Email or Nickname already exists!");
			modelAndView.addObject("user", user);
		}
		else {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "New user created!");
		
			JavaMailSenderImpl mailSender = emailConfig.newMailSender();
			
			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(user.getEmail());
			mailMessage.setSubject("New feedback from " + user.getName());
			mailMessage.setText("New user has been registered on BandMeUp whit this email.");
			
			//Send mail
			mailSender.send(mailMessage);
			modelAndView.addObject("user", new User());
		}
		
		modelAndView.setViewName("register");
		return modelAndView;
	}
	
	@GetMapping("/home")
	public String home(Model model) {
		return "home";
	}
	
	@GetMapping("/wedding")
	public String wedding(Model model) {
		List<Band> bands = (List<Band>) bandRepository.findWeddingSorted();
		Search search = new Search();
		model.addAttribute("bands", bands);
		model.addAttribute("search", search);
		return "bandList";
	}
	
	@GetMapping("/concert")
	public String concert(Model model) {
		List<Band> bands = (List<Band>) bandRepository.findConcertSorted();
		Search search = new Search();
		model.addAttribute("bands", bands);
		model.addAttribute("search", search);
		return "bandList";
	}
	
	@GetMapping("/party")
	public String party(Model model) {
		List<Band> bands = (List<Band>) bandRepository.findPartySorted();
		Search search = new Search();
		model.addAttribute("bands", bands);
		model.addAttribute("search", search);
		return "bandList";
	}
	
	@PostMapping("/bandList")
	public String search(Model model, @Valid Search search, BindingResult bindingResult) {
		if (search.getStr().isEmpty()) {
			return "redirect:/home";
		}
		String str = "%" + search.getStr() + "%";
		List<Band> bands = (List<Band>) bandRepository.search(str);
		model.addAttribute("bands", bands);
		return "bandList";
	}
	
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public ModelAndView adminHome() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("admin"); // resources/template/home.html
		return modelAndView;
	}
}
