package com.example.bands.controller;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.bands.configuration.EmailConfiguration;
import com.example.bands.model.Band;
import com.example.bands.model.Reservation;
import com.example.bands.model.Statistics;
import com.example.bands.model.Timechange;
import com.example.bands.model.Ts;
import com.example.bands.model.User;
import com.example.bands.repository.BandRepository;
import com.example.bands.repository.ReservationRepository;
import com.example.bands.repository.TimechangeRepository;
import com.example.bands.repository.UserRepository;
import com.example.bands.service.StatisticsService;
import com.example.bands.service.UserService;

@Controller
public class ReservationsController {

private EmailConfiguration emailConfig;
	
	public ReservationsController(EmailConfiguration emailConfig) {
		this.emailConfig = emailConfig;
	}
	
	@Autowired
	UserService userService;
	
	@Autowired
	StatisticsService statisticsService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BandRepository bandRepository;
	
	@Autowired
	private ReservationRepository reservationRepository;
	
	@Autowired
	private TimechangeRepository timechangeRepository;
	
	@RequestMapping(value = "/reserve/{id}", method = RequestMethod.GET)
	public ModelAndView reserve(@PathVariable int id) {
		ModelAndView modelAndView = new ModelAndView();
		Reservation reservation = new Reservation();
		Band band = bandRepository.getOne(id);
		modelAndView.addObject("reservation", reservation);
		modelAndView.addObject("band", band);
		modelAndView.addObject("ts", new Ts());
		modelAndView.setViewName("reserve"); 
		return modelAndView;
	}
	
	@RequestMapping(value = "/reserve/{id}", method = RequestMethod.POST)
	public ModelAndView reserveBand(@Valid Ts ts, BindingResult br,@Valid Reservation reservation, BindingResult bindingResult, @PathVariable("id") int id, @RequestParam String place, @RequestParam String address, @RequestParam String date, @RequestParam String hour, @RequestParam String type, @RequestParam String description, ModelMap modelMap, Authentication authentication) {
		ModelAndView modelAndView = new ModelAndView();
		Timestamp t = new Timestamp(System.currentTimeMillis());
		Band band = bandRepository.getOne(id);
		date = date + " " + hour +":00.0";
		Timestamp time = Timestamp.valueOf(date);
		if(bindingResult.hasErrors()) {
			modelAndView.addObject("errorMsg", "Please correct the errors in form!");
			modelMap.addAttribute("bindingResult", bindingResult);
		}
		else if(reservationRepository.isAvailable(id, time)) {
			modelAndView.addObject("errorMsg", "That term is already reserved! Pick another time.");
			modelAndView.addObject("reservation", reservation);
			modelAndView.addObject("band", band);
			modelAndView.addObject("ts", ts);
		}
		else if(time.before(t)) {
			modelAndView.addObject("errorMsg", "That term has passed! Please, pick a valid time.");
			modelAndView.addObject("reservation", reservation);
			modelAndView.addObject("band", band);
			modelAndView.addObject("ts", ts);
		}
		else if(type.equals("-1")) {
			modelAndView.addObject("errorMsg", "Please select a reservation type.");
			modelAndView.addObject("reservation", reservation);
			modelAndView.addObject("band", band);
			modelAndView.addObject("ts", ts);
		}
		else {
			User user = new User();
			user = userRepository.findByEmail(authentication.getName());
			
			Reservation r = new Reservation();
			r.setBandId(id);
			r.setUserId(user.getId());
			r.setPlace(place);
			r.setAddress(address);
			r.setTime(time);
			r.setType(type);
			r.setDescription(description);
			r.setVerified(0);
			reservationRepository.save(r);
			modelAndView.addObject("successMessage", "Reservation sent!");
		
			// Create mail sender
			JavaMailSenderImpl mailSender = emailConfig.newMailSender();
			
			
			
			User u = new User();
			u = userRepository.getOne(user.getId());
			//Create email instance
			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(band.getEmail());
			mailMessage.setSubject("New Reservation from " + u.getName());
			mailMessage.setText("You have one new reservation from user: " + u.getName() + "(" + u.getEmail() + ")" + ". Place: " + place + ". Time: " + time.toString() + ". Type: " + type);
			
			//Send mail
			mailSender.send(mailMessage);

			modelAndView.addObject("reservation", new Reservation());
			modelAndView.addObject("band", band);
			modelAndView.addObject("ts", new Ts());
		}
		modelAndView.setViewName("reserve");
		return modelAndView;
	}
	
	@GetMapping("/reservations")
	public String reservations(Model model, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Timestamp currTime = new Timestamp(System.currentTimeMillis());
		List<Reservation> pending = (List<Reservation>) reservationRepository.findMyPendingReservations(user.getId(), currTime);
		List<Reservation> verified = (List<Reservation>) reservationRepository.findMyVerifiedReservations(user.getId(), currTime);
		List<Reservation> timeChange = (List<Reservation>) reservationRepository.findMyTCReservations(user.getId(), currTime);
		List<Reservation> timeChangePending = (List<Reservation>) reservationRepository.findMyPendingTC(user.getId(), currTime);
		
		model.addAttribute("pending", pending);
		model.addAttribute("verified", verified);
		model.addAttribute("timeChange", timeChange);
		model.addAttribute("timeChangePending", timeChangePending);
		return "myReservations";
	}
	
	@GetMapping("/reservations/history")
	public String myHistory(Model model,  Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Timestamp time = new Timestamp(System.currentTimeMillis());
		int flag = 0;
		
		List<Reservation> reservations = (List<Reservation>) reservationRepository.findMyVerifiedHistory(user.getId(), time);
		
		model.addAttribute("reservations", reservations);
		model.addAttribute("flag", flag);
		return "history";
	}
	
	@GetMapping("/band/{id}/reservations")
	public String myBandsReservations(Model model, @PathVariable("id") int id, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		Timestamp currTime = new Timestamp(System.currentTimeMillis());
		
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		
		List<Reservation> pending = (List<Reservation>) reservationRepository.findBandsPendingReservations(id, currTime);
		List<Reservation> verified = (List<Reservation>) reservationRepository.findBandsVerifiedReservations(id, currTime);
		List<Reservation> timeChange = (List<Reservation>) reservationRepository.findBandsTCReservations(id, currTime);
		List<Reservation> timeChangePending = (List<Reservation>) reservationRepository.findBandsPendingTC(id, currTime);
		
		model.addAttribute("pending", pending);
		model.addAttribute("verified", verified);
		model.addAttribute("timeChange", timeChange);
		model.addAttribute("timeChangePending", timeChangePending);
		
		model.addAttribute("band", band);
		return "bandsReservations";
	}
	
	@GetMapping("/band/{id}/reservations/history")
	public String myBandsReservationsHistory(Model model, @PathVariable("id") int id, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		Timestamp time = new Timestamp(System.currentTimeMillis());
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int flag = 1;
		
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		
		List<Reservation> reservations = (List<Reservation>) reservationRepository.findBandsVerifiedHistory(id, time);
		
		List<Statistics> stat = (List<Statistics>) statisticsService.getStatistics(id, time);
		
		model.addAttribute("reservations", reservations);
		model.addAttribute("stat", stat);
		model.addAttribute("year", year);
		model.addAttribute("flag", flag);
		return "history";
	}
	
	@PostMapping("/band/{id}/reservations/{rid}/verify")
	public String verifyReservation(Model model, @PathVariable("id") int id, @PathVariable("rid") int rid, Authentication authentication) {
		User u = new User();
		u = userRepository.findByEmail(authentication.getName());
		Band b = bandRepository.getOne(id);
		Reservation reservation = reservationRepository.getOne(rid);
		User resUser = userRepository.getOne(reservation.getUserId());
		
		if(u.getId() != b.getCreatedBy()) {
			return "error";
		}
		
		if(reservationRepository.isAvailable(id, reservation.getTime())) {
			model.addAttribute("errorMsg", "That time is already reserved!");
			return "error";
		}
		else {
		reservationRepository.setVerified(rid);
		
		Band band = bandRepository.getOne(id);
		User user = userRepository.getOne(reservation.getUserId());
		
		JavaMailSenderImpl mailSender = emailConfig.newMailSender();
		
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(resUser.getEmail());
		mailMessage.setSubject(band.getName() + " has accepted your reservation!");
		mailMessage.setText("Your reservation: \n Place: " + reservation.getPlace() + "\n Time: " + reservation.getTime().toString() + "\n Type: " + reservation.getType() + "\n Has been accepted by " + band.getName());
		
		//Send mail
		mailSender.send(mailMessage);
		}
		
		return "redirect:/band/{id}/reservations";
	}
	
	@GetMapping("/change/{id}")
	public String changeForm(Model model, @PathVariable("id") int id, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Reservation res = reservationRepository.getOne(id);
		Band band = bandRepository.getOne(res.getBandId());
		
		// Only bands or reservations owner can change VERIFIED reservation
		if((user.getId() != res.getUserId() && user.getId() != band.getCreatedBy()) || res.getVerified() != 1) {
			return "error";
		}
		
		model.addAttribute("ts", new Ts());
		return "change";
	}
	
	@PostMapping("/change/{id}")
	public String changeCheckSubmit(@Valid Ts ts, BindingResult br, Model model, @PathVariable("id") int id, @RequestParam String date, @RequestParam String hour, Authentication authentication) {
		Timestamp ct = new Timestamp(System.currentTimeMillis());
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Reservation res = reservationRepository.getOne(id);
		Band band = bandRepository.getOne(res.getBandId());
		int flag = 0;
		date = date + " " + hour +":00.0";
		Timestamp time = Timestamp.valueOf(date);

		// Only bands or reservations owner can change VERIFIED reservation
		if((user.getId() != res.getUserId() && user.getId() != band.getCreatedBy()) || res.getVerified() != 1) {
			return "error";
		}
		if(reservationRepository.isAvailable(band.getId(), time)) {
			model.addAttribute("successMessage", "That term is already reserved! Pick another time.");
			model.addAttribute("ts", ts);
		}
		else if(time.before(ct)) {
			model.addAttribute("successMessage", "That term has passed! Please, pick a valid time.");
			model.addAttribute("ts", ts);
		}
		else {
			if(user.getId() == res.getUserId()) {
				flag = 3;
			}
			else {
				flag = 2;
			}
			
			Timechange t = new Timechange();
			t.setReservationId(res.getId());
			t.setTime(res.getTime());
			
			timechangeRepository.save(t);
			
			reservationRepository.updateTime(id, flag, time);

			model.addAttribute("successMessage", "Timechange request sent!");

			model.addAttribute("ts", new Ts());
		}
		return "change";
	}
	
	@PostMapping("/cancel/{id}")
	public String cancelReservation(Model model, @PathVariable("id") int id,Authentication authentication) {
		User user, bandUser = new User();
		user = userRepository.findByEmail(authentication.getName());
		Reservation res = reservationRepository.getOne(id);
		User resUser = userRepository.getOne(res.getUserId());
		Band band = bandRepository.getOne(res.getBandId());
		bandUser = userRepository.getOne(band.getCreatedBy());
		String ret = "error";
		

		// Only bands or reservations owner can cancel VERIFIED reservation
		if((user.getId() != res.getUserId() && user.getId() != band.getCreatedBy()) || res.getVerified() != 1) {
			return ret;
		}
		
		reservationRepository.deleteById(id);
		
		JavaMailSenderImpl mailSender = emailConfig.newMailSender();
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		
		if(user.getId() == res.getUserId()) {
			mailMessage.setTo(band.getEmail());
			mailMessage.setSubject(user.getNickname() + " has canceled the reservation.");
			mailMessage.setText("Reservation: \n Place: " + res.getPlace() + "\n Time: " + res.getTime().toString() + "\n Type: " + res.getType() + "\n Has been canceled by " + user.getNickname() + ". For more information you can contact the user on his email: " + user.getEmail() + ".");
			
			ret = "redirect:/reservations";
		}
		else if(user.getId() == band.getCreatedBy()) {
			mailMessage.setTo(resUser.getEmail());
			mailMessage.setSubject(band.getName() + " has canceled your reservation!");
			mailMessage.setText("Your reservation: \n Place: " + res.getPlace() + "\n Time: " + res.getTime().toString() + "\n Type: " + res.getType() + "\n Has been canceled by " + band.getName() + ". For more information you can contact the band on following email: " + bandUser.getEmail() + ".");

			ret = "redirect:/band/{id}/reservations";
		}
		
		mailSender.send(mailMessage);
		
		return ret;
	}
	
	@PostMapping("/decline/{id}")
	public String declineReservation(Model model, @PathVariable("id") int id, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Reservation res = reservationRepository.getOne(id);
		User resUser = userRepository.getOne(res.getUserId());
		Band band = bandRepository.getOne(res.getBandId());
		String ret = "error";
		

		// Only bands or reservations owner can cancel VERIFIED reservation
		if((user.getId() != res.getUserId() && user.getId() != band.getCreatedBy()) || (res.getVerified() != 0 && (res.getVerified() != 2 && res.getVerified() != 3))) {
			return ret;
		}
		

		JavaMailSenderImpl mailSender = emailConfig.newMailSender();
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		
		if(user.getId() == res.getUserId()) {
			mailMessage.setTo(band.getEmail());
			mailMessage.setSubject(user.getNickname() + " has declined the timechange request.");
			mailMessage.setText("For more informations visit http://localhost:8080/band/" + band.getId() + "/reservations");
			
			ret = "redirect:/reservations";
		}
		else {
			mailMessage.setTo(resUser.getEmail());
			mailMessage.setSubject(band.getName() + " has declined the timechange request.");
			mailMessage.setText("For more informations visit http://localhost:8080/reservations");

			ret = "redirect:/band/" + band.getId() + "/reservations";
		}
		
		if(res.getVerified() == 0) {
			reservationRepository.deleteById(id);
		}
		else {
			Timechange t = timechangeRepository.findByRid(id);
			reservationRepository.updateTime(id, 1, t.getTime());
			mailSender.send(mailMessage);
		}
		
		return ret;
	}
	
	@PostMapping("/accept/{id}")
	public String acceptTimechange(Model model, @PathVariable("id") int id, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Reservation res = reservationRepository.getOne(id);
		User resUser = userRepository.getOne(res.getUserId());
		Band band = bandRepository.getOne(res.getBandId());
		int bid = res.getBandId();
		String ret = "error";
		

		// Only bands or reservations owner can cancel VERIFIED reservation
		if((user.getId() != res.getUserId() && user.getId() != band.getCreatedBy()) || (res.getVerified() != 2 && res.getVerified() != 3)) {
			return ret;
		}

		JavaMailSenderImpl mailSender = emailConfig.newMailSender();
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		
		if(user.getId() == res.getUserId()) {
			mailMessage.setTo(band.getEmail());
			mailMessage.setSubject(user.getNickname() + " has accepted the timechange request.");
			mailMessage.setText("For more informations visit http://localhost:8080/band/" + band.getId() + "/reservations");
			
			ret = "redirect:/reservations";
		}
		else if(user.getId() == band.getCreatedBy()) {
			mailMessage.setTo(resUser.getEmail());
			mailMessage.setSubject(band.getName() + " has accepted the timechange request.");
			mailMessage.setText("For more informations visit http://localhost:8080/reservations");

			ret = "redirect:/band/" + bid + "/reservations";
		}
		
		mailSender.send(mailMessage);
		timechangeRepository.deleteByRid(id);
		reservationRepository.setVerified(id);
		
		return ret;
	}
}
