package com.example.bands.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.bands.model.PasswordChange;
import com.example.bands.model.Updates;
import com.example.bands.model.User;
import com.example.bands.repository.UserRepository;
import com.example.bands.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/profile")
	public String showMyProfile(Model model, Authentication authentication) {
		User user = new User();
		Updates updates = new Updates();
		user = userRepository.findByEmail(authentication.getName());
		model.addAttribute("user", user);
		model.addAttribute("updates", updates);
		return "profile";
	}
	
	@RequestMapping(value = "/change-password", method = RequestMethod.GET)
	public ModelAndView changePassword() {
		ModelAndView modelAndView = new ModelAndView();
		PasswordChange pc = new PasswordChange();
		modelAndView.addObject("pc", pc); 
		modelAndView.setViewName("changePassword"); // resources/template/register.html
		return modelAndView;
	}
	
	@RequestMapping(value = "/change-password", method = RequestMethod.POST)
	public ModelAndView updatePassword(@Valid PasswordChange pc, BindingResult bindingResult, @RequestParam String oldPassword, @RequestParam String newPassword, @RequestParam String verifiedPassword, ModelMap modelMap, Authentication authentication) {
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		if(!(userService.isPasswordCorrect(oldPassword, user.getId()))) {
			modelAndView.addObject("successMessage", "Wrong password!");
		}
		else if(!newPassword.equals(verifiedPassword)) {
			modelAndView.addObject("successMessage", "New password not matching! Try again.");
		}
		else {
			userService.updatePassword(newPassword, user.getId());
			modelAndView.addObject("successMessage", "Password changed!");
		}

		modelAndView.addObject("pc", pc); 
		modelAndView.setViewName("changePassword"); // resources/template/register.html
		return modelAndView;
	}
	
	@PostMapping(value = "/change-nickname")
	public String updateNickname(@Valid Updates updates, BindingResult bindingResult, @RequestParam String nickname, ModelMap modelMap, Authentication authentication, Model model) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		if(userService.isNicknameAlreadyPresent(nickname)) {
			model.addAttribute("user", user);
			model.addAttribute("updates", updates);
			model.addAttribute("errorMsg", "Nickname already present, pick another.");
			return "profile";
		}
		userRepository.updateNickname(nickname, user.getId());
		user = userRepository.findByEmail(authentication.getName());
		return "redirect:/profile";
	}
}
