package com.example.bands.controller;

import java.awt.Image;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.bands.model.Band;
import com.example.bands.model.Comment;
import com.example.bands.model.Gig;
import com.example.bands.model.Rating;
import com.example.bands.model.Updates;
import com.example.bands.model.User;
import com.example.bands.repository.BandRepository;
import com.example.bands.repository.CommentRepository;
import com.example.bands.repository.GigRepository;
import com.example.bands.repository.RatingRepository;
import com.example.bands.repository.ReservationRepository;
import com.example.bands.repository.TimechangeRepository;
import com.example.bands.repository.UserRepository;
import com.example.bands.service.UserService;

@Controller
public class BandsController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BandRepository bandRepository;
	
	@Autowired
	private ReservationRepository reservationRepository;
	
	@Autowired
	private CommentRepository commentRepository;
	
	@Autowired
	private RatingRepository ratingRepository;
	
	@Autowired
	private TimechangeRepository timechangeRepository;
	
	@Autowired
	private GigRepository gigRepository;
	
	@GetMapping("/new")
	public String newBandForm(Model model) {
		List<Gig> allGigs = (List<Gig>) gigRepository.findAll();
		model.addAttribute("band", new Band());
		model.addAttribute("allGigs", allGigs);
		return "newBandForm";
	}
	
	@PostMapping("/new")
	  public String bandCheckSubmit(@RequestParam String name, @RequestParam String email, @RequestParam String about, @RequestParam String phone, @RequestParam String city, @RequestParam String ytChannel, @RequestParam String bandPageUrl, @RequestParam String fbUrl, @RequestParam String ytUrl, @RequestParam String imgUrl, @RequestParam Set<Gig> gigs, @RequestParam int members, @Valid Band band, BindingResult bindingResult, Authentication authentication, Model model) throws IOException {
		if (bindingResult.hasErrors()) {
			model.addAttribute("band", band);
			List<Gig> allGigs = (List<Gig>) gigRepository.findAll();
			model.addAttribute("allGigs", allGigs);
		}
		else if(!isYoutubeUrl(ytUrl) && !ytUrl.isBlank()) {
			model.addAttribute("band", band);
			List<Gig> allGigs = (List<Gig>) gigRepository.findAll();
			model.addAttribute("allGigs", allGigs);
			model.addAttribute("errorMsg", "Youtube link not valid!");
		}
		else {
			User user = new User();
			user = userRepository.findByEmail(authentication.getName());
			
			ytUrl = "https://www.youtube.com/embed/" + getYtId(ytUrl);
			
			String img = imgUrl;
			imgUrl =img;
			
			Band b = new Band();
			b.setName(name);
			b.setEmail(email);
			b.setMembers(members);
			b.setAbout(about);
			b.setCreatedBy(user.getId());
			b.setGigs(gigs);
			b.setYtUrl(ytUrl);
			b.setImgUrl(imgUrl);
			b.setBandPageUrl(bandPageUrl);
			b.setPhone(phone);
			b.setCity(city);
			b.setYtChannel(ytChannel);
			b.setFbUrl(fbUrl);
			
			
			if(!checkImgUrl(img)) {
				b.setImgUrl("/images/no_image_available.jpg");
			}
			
			bandRepository.save(b);
	
			model.addAttribute("successMessage", "New band added!");
			List<Gig> allGigs = (List<Gig>) gigRepository.findAll();
			model.addAttribute("band", new Band());
			model.addAttribute("allGigs", allGigs);
		}
		return "newBandForm";
	  }
	
	@GetMapping("/info/{id}")
	public String showInfo(@PathVariable ("id") int id, Model model, Authentication authentication) {
		Band band;
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		band = bandRepository.getOne(id);
		boolean rated = ratingRepository.alreadyRated(band.getId(), user.getId());
		List<Comment> comments = (List<Comment>) commentRepository.findBandsComments(id);
		Comment comment = new Comment();
		Rating rating = new Rating();
		if(rated) {
			float rate = ratingRepository.getRated(band.getId(), user.getId());
			model.addAttribute("rate", rate);
			
		}
		model.addAttribute("band", band);
		model.addAttribute("comments", comments);
		model.addAttribute("comment", comment);
		model.addAttribute("rating", rating);
		model.addAttribute("rated", rated);
		return "moreInfo";
	}
	
	@PostMapping("/comment/{id}")
	public String comment(@PathVariable ("id") int id, @RequestParam String content, Model model, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		
		Timestamp time = new Timestamp(System.currentTimeMillis());
		
		Comment comm = new Comment();
		comm.setBandId(id);
		//comm.setUserId(user.getId());
		comm.setContent(content);
		comm.setTime(time);
		comm.setUser(user);
		
		commentRepository.save(comm);
		
		return "redirect:/info/{id}";
	}
	
	@PostMapping("/rate/{id}")
	public String rate(@PathVariable ("id") int id, @RequestParam float rating, Model model, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		
		Rating rat = new Rating();
		rat.setBandId(id);
		rat.setUserId(user.getId());
		rat.setRating(rating);

		if(ratingRepository.alreadyRated(id, user.getId())) {
			model.addAttribute("errorMsg", "You already rated!");
	        return "redirect:/info/{id}";
		}
		
		bandRepository.updateRating(rating, id);
		ratingRepository.save(rat);
		
		return "redirect:/info/{id}";
	}
	
	@GetMapping("/bands")
	public String myBands(Model model, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		List<Band> bands = (List<Band>) bandRepository.findMyBands(user.getId());
		model.addAttribute("bands", bands);
		return "myBands";
	}
	
	@GetMapping("/band/{id}")
	public String myBand(Model model, @PathVariable("id") int id, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		Updates updates = new Updates();
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		
		model.addAttribute("band", band);
		model.addAttribute("updates", updates);
		return "myBand";
	}
	
	@GetMapping("/delete/{id}")
	public String goToDelete(Model model, @PathVariable("id") int id, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		
		model.addAttribute("band", band);
		
		return "delete";
	}
	
	@PostMapping("/delete/{id}")
	public String delete(@PathVariable ("id") int id, Model model, Authentication authentication) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		
		commentRepository.deleteBand(id);
		ratingRepository.deleteBand(id);
		timechangeRepository.deleteBand(id);
		reservationRepository.deleteBand(id);
		gigRepository.deleteBand(id);
		bandRepository.deleteById(id);
		
		return "redirect:/bands";
	}
	
	@PostMapping(value = "/change-members/{id}")
	public String updateMembers(@PathVariable ("id") int id, @Valid Updates updates, BindingResult bindingResult, @RequestParam int members, ModelMap modelMap, Authentication authentication, Model model) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		else if (members == 0) {
			model.addAttribute("errorMsg", "Members can not be 0!");
			model.addAttribute("updates", updates);
		}
		else if(members == band.getMembers()) {
			model.addAttribute("errorMsg", "You entered the same number!");
			model.addAttribute("updates", updates);
		}
		else {
			bandRepository.updateMembers(members, id);
			band = bandRepository.getOne(id);
			model.addAttribute("sucessMsg", "Members updated!");
			model.addAttribute("updates", new Updates());
		}
		model.addAttribute("band", band);
		
		return "myBand";
	}
	
	@PostMapping(value = "/change-description/{id}")
	public String updateDescription(@PathVariable ("id") int id, @Valid Updates updates, BindingResult bindingResult, @RequestParam String description, ModelMap modelMap, Authentication authentication, Model model) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		else if (description.isBlank()) {
			model.addAttribute("errorMsg", "Description can not be empty!");
			model.addAttribute("updates", updates);
		}
		else if(description.equals(band.getAbout())) {
			model.addAttribute("errorMsg", "You entered existing description!");
			model.addAttribute("updates", updates);
		}
		else {
			bandRepository.updateDescription(description, id);
			band = bandRepository.getOne(id);
			model.addAttribute("sucessMsg", "Description updated!");
			model.addAttribute("updates", new Updates());
		}
		model.addAttribute("band", band);
		return "myBand";
	}
	
	@PostMapping(value = "/change-yt-url/{id}")
	public String updateYtLink(@PathVariable ("id") int id, @Valid Updates updates, BindingResult bindingResult, @RequestParam String ytUrl, ModelMap modelMap, Authentication authentication, Model model) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		else if((!isYoutubeUrl(ytUrl) || !ytUrl.isEmpty())) {
			model.addAttribute("errorMsg", "Youtube link is not valid!");
			model.addAttribute("updates", updates);
		}
		else {
			ytUrl = "https://www.youtube.com/embed/" + getYtId(ytUrl);
			bandRepository.updateYtUrl(ytUrl, id);
			band = bandRepository.getOne(id);
			model.addAttribute("sucessMsg", "Youtube link updated!");
			model.addAttribute("updates", new Updates());
		}
		model.addAttribute("band", band);
		return "myBand";
	}
	
	@PostMapping(value = "/change-img-url/{id}")
	public String updateImgLink(@PathVariable ("id") int id, @Valid Updates updates, BindingResult bindingResult, @RequestParam String imgUrl, ModelMap modelMap, Authentication authentication, Model model) throws IOException {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		else if (!checkImgUrl(imgUrl) || imgUrl.isBlank()) {
			model.addAttribute("errorMsg", "Image link is not valid!");
			model.addAttribute("updates", updates);
		}
		else if(imgUrl.equals(band.getImgUrl())) {
			model.addAttribute("errorMsg", "You entered existing image link!");
			model.addAttribute("updates", updates);
		}
		else {
			bandRepository.updateImgUrl(imgUrl, id);
			band = bandRepository.getOne(id);
			model.addAttribute("sucessMsg", "Band image link updated!");
			model.addAttribute("updates", new Updates());
		}
		model.addAttribute("band", band);
		return "myBand";
	}
	
	@PostMapping(value = "/change-page-url/{id}")
	public String updatePageLink(@PathVariable ("id") int id, @Valid Updates updates, BindingResult bindingResult, @RequestParam String bandPageUrl, ModelMap modelMap, Authentication authentication, Model model) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		else if (bandPageUrl.isBlank()) {
			model.addAttribute("errorMsg", "Band page link is not valid!");
			model.addAttribute("updates", updates);
		}
		else if(bandPageUrl.equals(band.getBandPageUrl())) {
			model.addAttribute("errorMsg", "You entered existing band page link!");
			model.addAttribute("updates", updates);
		}
		else {
			bandRepository.updatePageUrl(bandPageUrl, id);
			band = bandRepository.getOne(id);
			model.addAttribute("sucessMsg", "Band page link updated!");
			model.addAttribute("updates", new Updates());
		}
		model.addAttribute("band", band);
		return "myBand";
	}
	
	@PostMapping(value = "/change-fb-url/{id}")
	public String updateFbLink(@PathVariable ("id") int id, @Valid Updates updates, BindingResult bindingResult, @RequestParam String fbUrl, ModelMap modelMap, Authentication authentication, Model model) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		else if (!isFacebookUrl(fbUrl)) {
			model.addAttribute("errorMsg", "Facebook link is not valid!");
			model.addAttribute("updates", updates);
		}
		else if(fbUrl.equals(band.getFbUrl())) {
			model.addAttribute("errorMsg", "You entered existing Facebook link!");
			model.addAttribute("updates", updates);
		}
		else {
			bandRepository.updateFbUrl(fbUrl, id);
			band = bandRepository.getOne(id);
			model.addAttribute("sucessMsg", "Facebook link updated!");
			model.addAttribute("updates", new Updates());
		}
		model.addAttribute("band", band);
		return "myBand";
	}
	
	@PostMapping(value = "/change-yt-channel/{id}")
	public String updateYtChannel(@PathVariable ("id") int id, @Valid Updates updates, BindingResult bindingResult, @RequestParam String ytChannel, ModelMap modelMap, Authentication authentication, Model model) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		else if (!isYoutubeUrl(ytChannel)) {
			model.addAttribute("errorMsg", "Youtube link is not valid!");
			model.addAttribute("updates", updates);
		}
		else if(ytChannel.equals(band.getYtChannel())) {
			model.addAttribute("errorMsg", "You entered existing Youtube channel link!");
			model.addAttribute("updates", updates);
		}
		else {
			bandRepository.updateYtChannel(ytChannel, id);
			band = bandRepository.getOne(id);
			model.addAttribute("sucessMsg", "Youtube channel updated!");
			model.addAttribute("updates", new Updates());
		}
		model.addAttribute("band", band);
		return "myBand";
	}
	
	@PostMapping(value = "/change-phone/{id}")
	public String updatePhone(@PathVariable ("id") int id, @Valid Updates updates, BindingResult bindingResult, @RequestParam String phone, ModelMap modelMap, Authentication authentication, Model model) {
		User user = new User();
		user = userRepository.findByEmail(authentication.getName());
		Band band = bandRepository.getOne(id);
		if(user.getId() != band.getCreatedBy()) {
			return "error";
		}
		else if (phone.isBlank() || !isNumeric(phone)) {
			model.addAttribute("errorMsg", "Phone is not valid!");
			model.addAttribute("updates", updates);
		}
		else if(phone.equals(band.getPhone())) {
			model.addAttribute("errorMsg", "You entered existing phone number!");
			model.addAttribute("updates", updates);
		}
		else {
			bandRepository.updatePhone(phone, id);
			band = bandRepository.getOne(id);
			model.addAttribute("sucessMsg", "Phone updated!");
			model.addAttribute("updates", new Updates());
		}
		model.addAttribute("band", band);
		return "myBand";
	}
	
	public static boolean isYoutubeUrl(String youTubeURl)
	{
	       boolean success;
	       String pattern = "^(http(s)?:\\/\\/)?((w){3}.)?youtu(be|.be)?(\\.com)?\\/.+";
	       if (!youTubeURl.isEmpty() && youTubeURl.matches(pattern))
	       {
	           success = true;
	       }
	       else
	       {
	           success = false;
	       }
	       return success;
	}
	
	public static boolean isFacebookUrl(String fbUrl)
	{
	       boolean success;
	       String pattern = "((http|https)://)?(www[.])?facebook.com/.+";
	       if (!fbUrl.isEmpty() && fbUrl.matches(pattern))
	       {
	           success = true;
	       }
	       else
	       {
	           success = false;
	       }
	       return success;
	}
	
	public static String getYtId(String youTubeURl)
	{

		String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";
	
		Pattern compiledPattern = Pattern.compile(pattern);
		Matcher matcher = compiledPattern.matcher(youTubeURl);
		if (matcher.find()) {
		     return matcher.group().toString();
		}
		return "";
	}
	
	static boolean checkImgUrl(String url1) throws IOException {
		if(url1.isEmpty()) {
			return false;
		}
		
    	URL url = new URL(url1);
    	HttpURLConnection huc = (HttpURLConnection) url.openConnection();
    	huc.setRequestMethod("HEAD");
    	  
    	int responseCode = huc.getResponseCode();
    	  
    	if(responseCode != 200) {
    		return false;
    	}
    	
		Image image = ImageIO.read(url);
		
		if(image == null) {
			return false;
		}
		
		return true;
    }
	
	public static boolean isNumeric(String str) { 
		  try {  
		    Double.parseDouble(str);  
		    return true;
		  } catch(NumberFormatException e){  
		    return false;  
		  }  
		}
}
