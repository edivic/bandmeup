package com.example.bands.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.bands.model.User;


@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	public User findByEmail(String email);
	
	@Query(value = "SELECT * FROM auth_user WHERE nickname = ?1", 
			nativeQuery = true)
	public User findByNickname(String nickname);
	
	public User findByName(String name);
	
	@Query(value = "SELECT password FROM auth_user WHERE auth_user_id = ?1", 
			nativeQuery = true)
	public String getPassword(int id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE auth_user set password = ?1 where auth_user_id = ?2",
	nativeQuery = true)
	public int updatePassword(String password, int id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE auth_user set nickname = ?1 where auth_user_id = ?2",
	nativeQuery = true)
	public int updateNickname(String nickname, int id);
}
