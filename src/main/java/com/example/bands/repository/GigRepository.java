package com.example.bands.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.bands.model.Gig;

public interface GigRepository extends JpaRepository<Gig, Integer>{

	@Modifying
	@Transactional
	@Query(value="delete from band_gig where band_id = ?1",
	nativeQuery = true)
	public int deleteBand(int id);
}
