package com.example.bands.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.bands.model.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer>{

	@Query(value="select * from band_comments where band_id =?1",
			nativeQuery = true)
	public List<Comment> findBandsComments(int id);
	
	@Modifying
	@Transactional
	@Query(value="delete from band_comments where band_id = ?1",
	nativeQuery = true)
	public int deleteBand(int id);
}
