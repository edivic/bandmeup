package com.example.bands.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.bands.model.Timechange;

public interface TimechangeRepository extends JpaRepository<Timechange, Integer>{

	@Query(value="select * from timechange where reservation_id = ?1",
			nativeQuery = true)
	public Timechange findByRid(int id);
	
	@Modifying
	@Transactional
	@Query(value="delete from timechange where reservation_id = ?1",
	nativeQuery = true)
	public int deleteByRid(int id);
	
	@Modifying
	@Transactional
	@Query(value="delete from timechange where reservation_id = (select reservation_id from reservation where reservation_band_id = ?1)",
	nativeQuery = true)
	public int deleteBand(int id);
}
