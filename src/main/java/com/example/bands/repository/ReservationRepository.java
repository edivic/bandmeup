package com.example.bands.repository;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.bands.model.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

	@Query(value="select * from reservation where reservation_user_id =?1 and verified = 0 and reservation_time > ?2 order by reservation_time asc",
			nativeQuery = true)
	public List<Reservation> findMyPendingReservations(int id, Timestamp time);

	@Query(value="select * from reservation where reservation_user_id =?1 and  verified = 3 and reservation_time > ?2 order by reservation_time asc",
			nativeQuery = true)
	public List<Reservation> findMyPendingTC(int id, Timestamp time);
	
	@Query(value="select * from reservation where reservation_user_id =?1 and verified = 1 and reservation_time > ?2 order by reservation_time asc",
			nativeQuery = true)
	public List<Reservation> findMyVerifiedReservations(int id, Timestamp time);
	
	@Query(value="select * from reservation where reservation_user_id =?1 and verified = 2 and reservation_time > ?2 order by reservation_time asc",
			nativeQuery = true)
	public List<Reservation> findMyTCReservations(int id, Timestamp time);
	
	@Query(value="select * from reservation where reservation_user_id =?1 and verified = 1 and reservation_time < ?2 order by reservation_time asc",
			nativeQuery = true)
	public List<Reservation> findMyVerifiedHistory(int id, Timestamp time);
	
	@Query(value="select exists(select 1 from reservation where reservation_band_id = ?1 and reservation_time = ?2 and verified = 1)",
	nativeQuery = true)
	public boolean isAvailable(int id, Timestamp time);
	
	@Query(value="select * from reservation where reservation_band_id =?1 and verified = 0 and reservation_time > ?2 order by reservation_time asc",
			nativeQuery = true)
	public List<Reservation> findBandsPendingReservations(int id, Timestamp time);
	
	@Query(value="select * from reservation where reservation_band_id =?1 and verified = 2 and reservation_time > ?2 order by reservation_time asc",
			nativeQuery = true)
	public List<Reservation> findBandsPendingTC(int id, Timestamp time);
	
	@Query(value="select * from reservation where reservation_band_id =?1 and verified = 1 and reservation_time > ?2 order by reservation_time asc",
			nativeQuery = true)
	public List<Reservation> findBandsVerifiedReservations(int id, Timestamp time);
	
	@Query(value="select * from reservation where reservation_band_id =?1 and verified = 3 and reservation_time > ?2 order by reservation_time asc",
			nativeQuery = true)
	public List<Reservation> findBandsTCReservations(int id, Timestamp time);
	
	@Query(value="select * from reservation where reservation_band_id =?1 and verified = 1 and reservation_time < ?2 order by reservation_time asc",
			nativeQuery = true)
	public List<Reservation> findBandsVerifiedHistory(int id, Timestamp time);
	
	@Query(value="select count(*) from reservation where reservation_band_id =?1 and verified = 1 and extract(MONTH from reservation_time) = ?2 and extract(YEAR from reservation_time) >= ?3 and reservation_time < ?4",
			nativeQuery = true)
	public int findBandsMonthlyStatistics(int id, int month, int year, Timestamp time);
	
	@Modifying
	@Transactional
	@Query(value="update reservation set verified = 1 where reservation_id = ?1",
	nativeQuery = true)
	public int setVerified(int id);
	
	@Modifying
	@Transactional
	@Query(value="update reservation set reservation_time = ?3, verified = ?2 where reservation_id = ?1",
	nativeQuery = true)
	public int updateTime(int id, int flag ,Timestamp time);
	
	@Modifying
	@Transactional
	@Query(value="delete from reservation where reservation_band_id = ?1",
	nativeQuery = true)
	public int deleteBand(int id);

}
