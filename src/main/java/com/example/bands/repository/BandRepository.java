package com.example.bands.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.bands.model.Band;

public interface BandRepository extends JpaRepository<Band, Integer>{

	@Query(value="select * from band where created_by =?1",
			nativeQuery = true)
	public List<Band> findMyBands(int id);
	
	@Modifying
	@Transactional
	@Query(value="update band set rate_sum = ((rate_sum + ?1) / (no_rates + 1)), no_rates = no_rates + 1 where band_id = ?2",
	nativeQuery = true)
	public int updateRating(float rate, int id);
	
	@Query(value="select * from band order by rate_sum desc, no_rates desc",
			nativeQuery = true)
	public List<Band> findSorted();
	
	@Query(value="select * from band join band_gig on band.band_id = band_gig.band_id join gig on band_gig.gig_id = gig.gig_id where gig_type like 'Wedding' order by rate_sum desc, no_rates desc",
			nativeQuery = true)
	public List<Band> findWeddingSorted();
	
	@Query(value="select * from band join band_gig on band.band_id = band_gig.band_id join gig on band_gig.gig_id = gig.gig_id where gig_type like 'Concert' order by rate_sum desc, no_rates desc",
			nativeQuery = true)
	public List<Band> findConcertSorted();
	
	@Query(value="select * from band join band_gig on band.band_id = band_gig.band_id join gig on band_gig.gig_id = gig.gig_id where gig_type like 'Party' order by rate_sum desc, no_rates desc",
			nativeQuery = true)
	public List<Band> findPartySorted();
	
	@Query(value="select * from band where lower(band_name) like lower(?1)",
			nativeQuery = true)
	public List<Band> search(String str);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE band set members = ?1 where band_id = ?2",
	nativeQuery = true)
	public int updateMembers(int members, int id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE band set about = ?1 where band_id = ?2",
	nativeQuery = true)
	public int updateDescription(String about, int id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE band set yt_url = ?1 where band_id = ?2",
	nativeQuery = true)
	public int updateYtUrl(String url, int id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE band set img_url = ?1 where band_id = ?2",
	nativeQuery = true)
	public int updateImgUrl(String url, int id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE band set band_page_url = ?1 where band_id = ?2",
	nativeQuery = true)
	public int updatePageUrl(String url, int id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE band set fb_url = ?1 where band_id = ?2",
	nativeQuery = true)
	public int updateFbUrl(String url, int id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE band set yt_channel = ?1 where band_id = ?2",
	nativeQuery = true)
	public int updateYtChannel(String url, int id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE band set phone = ?1 where band_id = ?2",
	nativeQuery = true)
	public int updatePhone(String phone, int id);
}
