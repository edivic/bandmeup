package com.example.bands.repository;


import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.bands.model.Rating;

public interface RatingRepository extends JpaRepository<Rating, Integer>{

	@Query(value="select exists(select 1 from band_ratings where rating_band_id = ?1 and rating_user_id = ?2)",
			nativeQuery = true)
			public boolean alreadyRated(int b_id, int u_id);
	
	@Modifying
	@Transactional
	@Query(value="delete from band_ratings where rating_band_id = ?1",
	nativeQuery = true)
	public int deleteBand(int id);
	
	@Query(value="select rating from band_ratings where rating_band_id = ?1 and rating_user_id = ?2",
			nativeQuery = true)
			public float getRated(int b_id, int u_id);
}
